define(['views/app'], function (
            AppView
        ){

    'use strict';
    return new AppView();
});