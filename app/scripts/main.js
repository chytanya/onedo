require.config({
    paths: {
        jquery: '../components/jquery/jquery',
        bootstrap: 'vendor/bootstrap',
        moment : '../components/moment/moment',
        underscore : '../components/underscore/underscore',
        backbone : '../components/backbone-min/index',
        fastClick : '../components/fastclick/lib/fastclick',
        'backbone.localStorage' : 'vendor/backbone.localStorage',
        text : '../components/text/text',
        hammerjs : '../components/hammerjs/dist/hammer.min',
        hammer : '../components/hammerjs/dist/jquery.hammer.min'
    },
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        backbone : {
            deps : ['underscore'],
            exports : 'Backbone'
        },
        underscore: {
            deps : ['jquery'],
            exports : '_'
        },
        fastClick: {
            deps : ['jquery'],
            exports : 'FastClick'
        },
        localStorage: {
            deps : ['underscore', 'backbone'],
            exports : 'Backbone'
        },
        hammer: {
            deps : ['jquery', 'hammerjs'],
            exports : 'Hammer'
        }
    }
});

require(['jquery', 'moment', 'fastClick', 'bootstrap', 'hammer', 'app'], function ($, moment, FastClick, Bootstrap, Hammer, appView) {
    
    'use strict';

    $(function() {
        FastClick.attach(document.body);
    });

    appView.showCalendar();

});