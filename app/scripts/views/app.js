define([
        'moment',
        'backbone',
        'collections/todos',
        'views/calendar', 'views/todo',
        'text!../../templates/header.html',
        'text!../../templates/calendar.html',
        'text!../../templates/todo.html',
        'text!../../templates/no_todo.html',
        'text!../../templates/add_todo.html',
        'text!../../templates/new_task/header.html',
        'text!../../templates/new_task/form.html',
        'text!../../templates/todo_preview.html'
        ], function (
            moment,
            Backbone,
            Todos,
            CalendarView, TodoView,
            headerTemplateHTML,
            calendarTemplateHTML,
            todoTemplateHTML,
            noTodoTemplateHTML,
            addTodoTemplateHTML,
            newTodoHeaderTemplateHTML,
            newTodoFormTemplateHTML,
            todoPreviewTemplateHTML
        ){

    'use strict';

    var currentDate     =   moment(),
        firstDate       =   moment(),
        lastDate        =   moment();
    
    var AppView = Backbone.View.extend({
        
        el : $('#app'),

        headerTemplate : _.template(headerTemplateHTML),
        
        calendarTemplate : _.template(calendarTemplateHTML),
        
        todoTemplate : _.template(todoTemplateHTML),

        noTodoTemplate : _.template(noTodoTemplateHTML),

        addTodoTemplate : _.template(addTodoTemplateHTML),

        newTodoHeaderTemplate : _.template(newTodoHeaderTemplateHTML),

        newTodoFormTemplate : _.template(newTodoFormTemplateHTML),

        todoPreviewTemplate : _.template(todoPreviewTemplateHTML),

        events: {
            'click #footer #show-calendar'      :   'showCalendar',
            'swipe #calendar'                   :   'changeMonth',
            'click #calendar .date'             :   'selectDate',
            'click #add-new .btn'               :   'addNewTaskForDate',
            'click #cancel-todo'                :   'closeTodoForm',
            'click #header .create-todo-btn'    :   'createNewTodo',
            'click #todo-preview #show-full-todo'   :   'showFullTodo',
            'swipe #todo-preview'               :   'swipeShowFullTodo',
            'click #todo-preview'               :   'hideTodoOptions',
            'click #todo-preview #mark-complete':   'completeAndArchiveTodo'
        },
        
        initialize : function(){

            this.$header = this.$('#header');
            this.$input = this.$('#new-todo');
            this.$footer = this.$('#footer');
            this.$todo  =   this.$('#todo');
            this.$main = this.$('#main');

            //this.listenTo(Todos, 'add', this.addOne);
            //this.listenTo(Todos, 'reset', this.addAll);
            //this.listenTo(Todos, 'change:completed', this.filterOne);
            //this.listenTo(Todos, 'add', this.showCalendar);

            Todos.fetch();

            this.$el.hammer();
        },
        
        render: function(){
            if(Todos.length == 0){
                this.$main.html(this.noTodoTemplate({}));  
            }
        },

        showCalendarForDate: function(date){

            currentDate     =   moment(date, 'MM-DD-YYYY');
            firstDate       =   moment(date, 'MM-DD-YYYY');
            lastDate        =   moment(date, 'MM-DD-YYYY');
            this.showCalendar();
        },

        showCalendar: function(){

            var dayLabels = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

            while(firstDate.format('DD') != '01'){
                firstDate.subtract('days', 1);
            }

            lastDate    =   firstDate.day(1).clone();
            lastDate.add('days', 34);
            
            $('#header #title').remove();
            this.$header.html(this.headerTemplate({ title : currentDate.format('MMM YYYY') }));
            $('#header #title').addClass('animated fadeInDown');

            var datesWithTodos = Todos.pluck("due");

            this.$main.html(this.calendarTemplate({ 
                                firstDate   :   firstDate.day(1), 
                                lastDate    :   lastDate, 
                                currentDate :   currentDate,
                                dayLabels   :   dayLabels,
                                datesWithTodos : datesWithTodos
                            }));
            this.showTodoForSelectedDate(currentDate.format('MM-DD-YYYY'));
        },

        // date format: MM-DD-YYYY
        showTodoForSelectedDate: function(date){
            
            if(!date)  date = moment().format('MM-DD-YYYY');
            
            var todoForSelectedDate = Todos.findWhere({ due  : date });

            this.$main.find('#todo').removeClass('animated fadeInUp');

            if(todoForSelectedDate){
                this.$main.find('#todo')
                    .html(this.todoPreviewTemplate({ todo : todoForSelectedDate.toJSON() }))
            }else{
                this.$main.find('#todo').html(this.addTodoTemplate({}));
            }

            this.$main.find('#todo').addClass('animated fadeInUp');

        },

        showNextMonth: function(){
            this.$main.find('#calendar').addClass('animated fadeOutLeft');
            currentDate.add('months', 1);
            firstDate           =   currentDate.clone();
            lastDate            =   currentDate.clone();
            this.showCalendar();
            $('#calendar').addClass('animated bounceInRight');
        },

        showPrevMonth: function(){
            this.$main.find('#calendar').addClass('animated fadeOutRight');
            currentDate.subtract('months', 1);
            firstDate           =   currentDate.clone();
            lastDate            =   currentDate.clone();
            this.showCalendar();
            $('#calendar').addClass('animated bounceInLeft');
        },

        changeMonth: function(e){                
            if(e.gesture.direction == 'left'){
                this.showNextMonth();
            }else if(e.gesture.direction == 'right'){
                this.showPrevMonth();
            }else{

            }
        },

        selectDate: function(e){
            $('.date').removeClass('selected');
            var date            =   $(e.target).closest('.date');
            $(date).addClass('selected');
            var selectedDate    =   $('.date.selected').attr('data-full-date');
            this.showTodoForSelectedDate(selectedDate);
        },

        addNewTaskForDate: function(e){

            if($('.date.selected').length == 0){
                alert('Select a date first');
                return false;
            }

            var selectedDate    =   $('.date.selected').attr('data-full-date').trim();
            var formattedDate   =   moment(selectedDate, 'MM-DD-YYYY').format('MMMM Do, YYYY');

            this.$header.html(this.newTodoHeaderTemplate({}));

            $('#todo')
                    .html(this.newTodoFormTemplate({ date : selectedDate, formattedDate : formattedDate }))
                    .removeClass('animated fadeInDown')
                    .addClass('fullscreen animated fadeInUp');
        },

        closeTodoForm: function(){
            this.$header.html(this.headerTemplate({ title : currentDate.format('MMM YYYY') }));
            $('#todo')
                    .html(this.addTodoTemplate({}))
                    .removeClass('fullscreen fadeInUp')
                    .addClass('fadeInDown');
        },

        createNewTodo: function(){

            if(!$('#new-todo-form .title').val().trim()){
                alert('A title for todo is required.');
                return false;
            }

            var dueDate = $('#new-todo-form .date').attr('data-date');

            Todos.create({
                text : $('#new-todo-form .title').val(),
                notes : $('#new-todo-form .notes').val(),
                created : moment().format('MM-DD-YYYY'),
                due : dueDate
            });

            this.showCalendarForDate(dueDate);
        },


        swipeShowFullTodo: function(e){

            if(e.gesture.direction == 'up'){
                this.showFullTodo();
            }

            if(e.gesture.direction == 'down'){
                this.showTodoOptions();
            }
        },

        showFullTodo: function(){


            var dateString  =   $('.date.selected').attr('data-full-date');
            var todoForSelectedDate = Todos.findWhere({ due  : dateString });

            $('#todo')
                .addClass('fullscreen')
                .html(this.todoTemplate({ todo  : todoForSelectedDate.toJSON() }));

        },

        showTodoOptions: function(){
            $('#todo-preview .completed-wrapper').show();
        },

        hideTodoOptions: function(){
            $('#todo-preview .completed-wrapper').hide();
        },

        completeAndArchiveTodo: function(e){

            var todoId = $(e.target).closest('#todo-preview').attr('data-todo-id');
            console.log('the todo id', todoId);
            var todo = Todos.get(todoId);
            if(!todo){
                alert('Cant find todo!!');
                return false;
            }
            todo.set('completed', moment().format('MM-DD-YYYY'));
            this.updateArchive(todo.toJSON());
            var todoDate = todo.get('due');
            todo.destroy();

            this.showCalendarForDate(todoDate);

        },

        updateArchive: function(todo){
            // stored at app.archive.todos

            var archivedTodosNamespace = 'app.archive.todos';
            var archivedTodos = localStorage.getItem(archivedTodosNamespace);
            if(!archivedTodos) archivedTodos = [];
            else archivedTodos = JSON.parse(archivedTodos);
            archivedTodos.push(todo);
            localStorage.setItem(archivedTodosNamespace, JSON.stringify(archivedTodos));
        }
        
    });

    return AppView;
});